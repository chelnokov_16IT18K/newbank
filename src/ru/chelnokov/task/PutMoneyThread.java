package ru.chelnokov.task;

public class PutMoneyThread extends Thread {
    private final Account bankAccount;
    private int sum;//сумма пополнения

    /**
     * Конструктор для объекта-потока для внесения денег
     *
     * @param bankAccount объект типа BankAccount
     * @param sum         сумма внесения
     */
    PutMoneyThread(Account bankAccount, int sum) {
        this.bankAccount = bankAccount;
        this.sum = sum;
    }

    /**
     * Метод запуска потока
     */
    public void run() {
        synchronized (bankAccount) {
            bankAccount.waitingToTakeOff(sum);
        }
    }
}
