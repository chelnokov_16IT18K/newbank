package ru.chelnokov.task;

public class Main {
    public static void main(String[] args) {
        Account bankAccount = new Account(0);
        System.out.println("Начальный баланс: " + bankAccount.getBalance());
        PutMoneyThread putMoneyThread = new PutMoneyThread(bankAccount, 7);
        putMoneyThread.start();
        try {
            putMoneyThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Остаток после снятия: " + bankAccount.getBalance());
    }
}
