package ru.chelnokov.task;

class Account {
    private int balance;

    /**
     * Кооструктор объекта аккаунт банка
     *
     * @param balance текущий баланс
     */
    Account(int balance) {
        this.balance = balance;
    }

    void waitingToTakeOff(int sum) {
        int minusSum = 380;
        while (balance < minusSum) {
            put(sum);
            System.out.println("+" + sum + " рублей!");
            System.out.println("Теперь ваш баланс: " + balance);
        }
        takeOff(minusSum);
    }

    /**
     * Метод для внесения денег на счёт
     *
     * @param sum сумма внесения
     */
    private synchronized void put(int sum) {
        this.balance = balance + sum;
    }


    /**
     * Метод для снятия денег со счёта
     *
     * @param sum сумма снятия
     */
    private synchronized void takeOff(int sum) {
        while (balance >= sum) {
            this.balance = balance - sum;
            System.out.println("-" + sum + " рублей!");
        }
    }

    /**
     * Метод для передачи значения баланса
     *
     * @return текущий баланс
     */
    int getBalance() {
        return balance;
    }
}
